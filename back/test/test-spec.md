> import Lib
> import Data.Function
> import Data.Time
> import Test.Hspec hiding (after, before)

> workday1 = WorkPeriod (LocalTime (fromGregorian 2020 1 1) (time 8 0)) (LocalTime (fromGregorian 2020 1 1) (time 18 0))

> michon = mission "Lever Mme Michon" & during 1 0 & before 10 0
> bidochon = mission "Repas M. Bidochon" & during 0 30 & after 11 30 & before 13 0


> main :: IO ()
> main = hspec $ do
>   describe "work day" $ do


Une journée vide est OK.

>     it "an empty workload should always be correct" $ do
>       validateWorkLoad workday1 [] `shouldBe` Success
>
>     it "a single mission occuring before start of workday should be incorrect" $ do
>       let michon' = michon & before 7 0
>       validateWorkLoad workday1 [michon'] `shouldBe` MissionTooEarly
>
>     it "a single mission occuring after end of workday should be incorrect" $ do
>       validateWorkLoad workday1 [michon & after 19 0] `shouldBe` MissionTooLate
>
>     it "a mission occuring after end of workday should be incorrect" $ do
>       validateWorkLoad workday1 [bidochon, michon & after 19 0] `shouldBe` MissionTooLate

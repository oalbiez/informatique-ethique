module Lib where

import           Control.Monad.Writer
import           Data.Function
import           Data.Maybe
import           Data.Monoid
import           Data.Time.LocalTime
import           Data.Time
import           Data.Function                  ( (&) )

type TimeDuration = NominalDiffTime

durationInTime :: Int -> Int -> TimeDuration
durationInTime hours minutes =
  secondsToNominalDiffTime $ fromIntegral $ hours * 3600 + minutes * 60


data Mission = Mission
  { m_title :: String
  , m_duration :: TimeDuration
  , m_address :: Address
  , m_atEarliest :: Maybe TimeOfDay
  , m_atLatest :: Maybe TimeOfDay
  } deriving (Show)

instance HasDuration Mission where
  duration = m_duration

data Travel =  Travel
  { t_from :: Address
  , t_to :: Address
  , t_duration :: TimeDuration
  } deriving (Show)

instance HasDuration Travel where
  duration = t_duration

-- data Pause = Pause 
--  { p_duration :: TimeDuration
--  } deriving (Show)

-- instance HasDuration Pause where
--   duration = p_duration

class HasDuration a where
  duration :: a -> TimeDuration


type Address = String


data WorkPeriod = WorkPeriod {start :: LocalTime, end :: LocalTime }

shiftPeriod wp diff = wp { start = addLocalTime diff $ start wp }


data ValidationResult = Success
                      | Errors [ValidationResult]
                      | MissionTooLong
                      | MissionTooEarly
                      | MissionTooLate
                        deriving (Eq, Show) -- MissionBeforeWorkPeriod Mission

combineResults :: ValidationResult -> ValidationResult -> ValidationResult
combineResults r1 r2 = case (r1, r2) of
  (Success  , _        ) -> r2
  (Errors _ , Success  ) -> r1
  (Errors e1, Errors e2) -> Errors $ e1 ++ e2
  (Errors e1, _        ) -> Errors $ e1 ++ [r2]
  (_        , Errors e2) -> Errors $ [r1] ++ e2
  (_        , Success  ) -> r1
  _                      -> Errors [r1, r2]

instance Semigroup ValidationResult where
  (<>) = combineResults

instance Monoid ValidationResult where
  mempty = Success


type Validator = Writer ValidationResult ()

orElse :: ValidationResult -> Bool -> Validator
orElse fail cond = tell $ if cond then Success else fail

maybeCheck :: (a -> Validator) -> Maybe a -> Validator
maybeCheck fun mval = maybe (tell Success) fun mval


runValidator :: Validator -> ValidationResult
runValidator = snd . runWriter

(|>) = (&)

validateWorkLoad
  :: (Address -> Address -> Maybe Travel)
  -> WorkPeriod
  -> [Mission]
  -> Validator
validateWorkLoad _      _  []             = tell Success
validateWorkLoad oracle wp (e1 : e2 : es) = do
  (addLocalTime (duration e1) (start wp) <= end wp) |> orElse MissionTooLong
  maybeCheck
      (\time -> orElse MissionTooEarly $ time >= (localTimeOfDay $ start wp))
    $ m_atLatest e1
  maybeCheck
      (\time -> orElse MissionTooLate $ time <= (localTimeOfDay $ end wp))
    $ m_atEarliest e1


  validateWorkLoad oracle (shiftPeriod wp (duration e1)) (e2 : es)

validateWorkLoad oracle wp (e : es) = do
  (addLocalTime (duration e) (start wp) <= end wp) |> orElse MissionTooLong
  maybeCheck
      (\time -> orElse MissionTooEarly $ time >= (localTimeOfDay $ start wp))
    $ m_atLatest e
  maybeCheck
      (\time -> orElse MissionTooLate $ time <= (localTimeOfDay $ end wp))
    $ m_atEarliest e

  validateWorkLoad oracle (shiftPeriod wp (duration e)) es

module GenericTest exposing (Model, Msg, baseModel, score, update, view)

import Browser
import Element as E
import Element.Border as EBd
import Element.Events as E
import Element.Font as EF
import Element.Input as In
import Html exposing (Html, a, button, div, h1, img, input, span, text)
import Html.Attributes exposing (src, style)
import Html.Events exposing (onClick, onInput)
import Maybe exposing (Maybe)



---- MODEL ----


type alias Model =
    { patient : String
    , reponses : List UneReponse
    }


type alias UneReponse =
    { test : String
    , valeur : ( String, Int )
    }


baseModel : Model
baseModel =
    { patient = "Charline", reponses = [] }


score : Model -> Int
score model =
    model.reponses
        |> List.map .valeur
        |> List.map Tuple.second
        |> List.sum



---- UPDATE ----


type Msg
    = Aucun
    | Patient String
    | Reponse String ( String, Int )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Reponse testName valeur ->
            let
                nouvellesReponses =
                    model.reponses
                        |> List.filter keepOthers
                        |> ajouteNouveauTest

                ajouteNouveauTest reponses =
                    { test = testName
                    , valeur = valeur
                    }
                        :: reponses

                keepOthers reponse =
                    reponse.test /= testName
            in
            ( { model | reponses = nouvellesReponses }
            , Cmd.none
            )

        Patient nomPatient ->
            ( { model | patient = nomPatient }, Cmd.none )

        _ ->
            ( model, Cmd.none )



---- VIEW ----


view : (Model -> E.Element Msg) -> List TestData -> Model -> E.Element Msg
view diagnostic testsData model =
    E.column [ E.spacing 10, E.padding 40 ] <|
        [ E.el [] <| E.text "GET UP AND GO TEST"
        , patientSelectionne model
        , diagnostic model
        ]
            ++ List.map (viewTest model.reponses) testsData


patientSelectionne : Model -> E.Element Msg
patientSelectionne model =
    In.text []
        { onChange = Patient
        , text = model.patient
        , placeholder = Nothing
        , label = In.labelLeft [] <| E.text "Nom Patient: "
        }


type alias TestData =
    { consigne : String
    , options : List ( String, Int )
    }


viewTest : List UneReponse -> TestData -> E.Element Msg
viewTest reponses data =
    let
        choiceButton ( reponse, valeur ) =
            E.html <|
                button
                    [ onClick (Reponse data.consigne ( reponse, valeur ))
                    , reponses
                        |> List.filter (\element -> element == { test = data.consigne, valeur = ( reponse, valeur ) })
                        |> List.length
                        |> (\nombre -> nombre > 0)
                        |> boldIfTrue
                    ]
                    [ text reponse ]
    in
    E.column [ E.padding 20, E.spacing 10, EBd.width 1, E.width E.fill ] <|
        [ E.el [] <| E.text data.consigne
        , E.row [] <| List.map choiceButton data.options
        ]


boldIfTrue : Bool -> Html.Attribute msg
boldIfTrue cond =
    if cond then
        style "font-weight" "bold"

    else
        style "font-weight" "normal"

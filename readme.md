[![pipeline status](https://gitlab.com/oalbiez/informatique-ethique/badges/master/pipeline.svg)](https://gitlab.com/oalbiez/informatique-ethique/commits/master)

# ethic-router





# TODO

Ce qui peut impacter le temps passé chez le bénéficiaire :

1. L'habitude chez le bénéficiaire :
   - localiser le logement du bénéficiaire
   - sociabiliser avec la personne qui ne connaît pas forcément l'intervenant
   - trouver où sont rangés les affaires
2. La compétence (ex: formé à la toilette au lit)
3. Une mission pourrait effectivement varié dans l'horaire de démarrage
4. L'état du bénéficiaire. S'il ne va pas bien, il y a un risque de devoir rester un peu plus longtemps. Cela peut d'anticiper d'une journée à l'autre

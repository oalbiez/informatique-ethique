module GetUpAndGo exposing (diagnostic, testItems)

import Element as E
import GenericTest as GT


diagnostic : GT.Model -> E.Element msg
diagnostic model =
    let
        etatPatient =
            if GT.score model < 2 then
                "Aucune instabilité"

            else if GT.score model < 8 then
                "Risque de chute modéré"

            else
                "Risque de chute élevé"
    in
    E.row []
        [ E.text ("score : " ++ String.fromInt (GT.score model))
        , E.text " - "
        , E.text etatPatient
        ]


testLeverData =
    { consigne = "Se lever du siège"
    , options =
        [ ( "Patient se rejette en arrière", 4 )
        , ( "Patient se penche en avant de manière anormale", 0 )
        , ( "Obligé de s'aider des accoudoirs", 2 )
        , ( "Se lève d'un seul élan", 0 )
        , ( "Besoin de deux ou trois essais", 1 )
        ]
    }


testMarcheData =
    { consigne = "Marcher devant soi trois mètres"
    , options =
        [ ( "Marche rectiligne sans détour", 0 )
        , ( "Méandres prononcés", 1 )
        ]
    }


testItems =
    [ testLeverData, testMarcheData ]

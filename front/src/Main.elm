module Main exposing (..)

import Browser
import Cmd.Extra exposing (withNoCmd)
import Element as E
import Element.Border as EBd
import Element.Events as E
import Element.Font as EF
import GenericTest as GT
import GetUpAndGo
import Glasgow
import Html exposing (Html, div, h1, img, span, text)
import Html.Attributes exposing (src, style)
import Html.Events exposing (onClick)
import Maybe exposing (Maybe)
import Planning



---- MODEL ----


type Model
    = Index
    | GlasgowModel GT.Model
    | GetUpAndGoModel GT.Model
    | PlanningModel Planning.Model


init : ( Model, Cmd Msg )
init =
    Index |> withNoCmd



---- UPDATE ----


type Msg
    = Navigate Page
    | GlasgowMsg GT.Model GT.Msg
    | GetUpAndGoMsg GT.Model GT.Msg
    | PlanningMsg Planning.Model Planning.Msg


type Page
    = PageGlasgow
    | PageGetUpAndGo
    | PagePlanning


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Navigate PageGlasgow ->
            GlasgowModel GT.baseModel |> withNoCmd

        Navigate PageGetUpAndGo ->
            GetUpAndGoModel GT.baseModel |> withNoCmd

        Navigate PagePlanning ->
            PlanningModel Planning.init |> withNoCmd

        GlasgowMsg oldSubModel subMsg ->
            let
                ( subModel, cmd ) =
                    GT.update subMsg oldSubModel
            in
            ( GlasgowModel subModel, Cmd.map (GlasgowMsg subModel) cmd )

        PlanningMsg oldSubModel subMsg ->
            let
                ( subModel, cmd ) =
                    Planning.update subMsg oldSubModel
            in
            ( PlanningModel subModel, Cmd.map (PlanningMsg subModel) cmd )

        GetUpAndGoMsg oldSubModel subMsg ->
            let
                ( subModel, cmd ) =
                    GT.update subMsg oldSubModel
            in
            ( GetUpAndGoModel subModel, Cmd.map (GetUpAndGoMsg subModel) cmd )



---- VIEW ----


view : Model -> Html Msg
view model =
    let
        specificView =
            case model of
                GlasgowModel subModel ->
                    E.map (GlasgowMsg subModel) <| GT.view Glasgow.diagnostic Glasgow.testItems subModel

                PlanningModel subModel ->
                    E.map (PlanningMsg subModel) <| E.html <| Planning.view subModel

                GetUpAndGoModel subModel ->
                    E.map (GetUpAndGoMsg subModel) <| GT.view GetUpAndGo.diagnostic GetUpAndGo.testItems subModel

                _ ->
                    E.none
    in
    E.layout [] <|
        E.column []
            [ E.row
                [ E.padding 40, E.spacing 20 ]
                [ E.el [ E.onClick <| Navigate PageGlasgow ] <| E.text "Glasgow"
                , E.el [ E.onClick <| Navigate PagePlanning ] <| E.text "Planning"
                , E.el [ E.onClick <| Navigate PageGetUpAndGo ] <| E.text "Get up and go"
                ]
            , specificView
            ]



---- PROGRAM ----


main : Program () Model Msg
main =
    Browser.element
        { view = view
        , init = \_ -> init
        , update = update
        , subscriptions = always Sub.none
        }

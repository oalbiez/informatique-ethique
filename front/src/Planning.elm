module Planning exposing (..)

import Browser
import Element as E
import Element.Border as EBd
import Element.Events as E
import Element.Font as EF
import Html exposing (Html, div, h1, img, span, text)
import Html.Attributes exposing (src, style)
import Html.Events exposing (onClick)
import Maybe exposing (Maybe)



---- MODEL ----


type alias TimeOfDay =
    { hours : Int
    , minutes : Int
    }


todFromHours : Int -> TimeOfDay
todFromHours hours =
    { hours = hours
    , minutes = 0
    }


type alias Mission =
    { name : String
    , description : String
    , duration : Int
    , startTime : TimeOfDay
    , every : Frequency
    }


type Day
    = Mon
    | Tue
    | Wed
    | Thu
    | Fry
    | Sat
    | Sun


type Frequency
    = Once
    | Daily
    | Days (List Day)


showFreq : Frequency -> String
showFreq frequency =
    case frequency of
        Once ->
            "Once"

        Daily ->
            "Daily"

        Days list ->
            "Some days"


type alias Model =
    { theTime : TimeOfDay
    , missionPool : List Mission
    , acceptedMissions : List Mission
    , currentSelection : Maybe Mission
    }


dummyTime =
    { hours = 8
    , minutes = 0
    }


init : Model
init =
    { theTime = { hours = 3, minutes = 12 }
    , missionPool =
        [ { name = "M. Bidochon", description = "Aide au lever", duration = 200, startTime = dummyTime, every = Daily }
        , { name = "Mme. Michon", description = "Aide au repas", duration = 200, startTime = dummyTime, every = Daily }
        , { name = "M. Dupond", description = "Compagnie", duration = 200, startTime = dummyTime, every = Daily }
        ]
    , acceptedMissions = []
    , currentSelection = Nothing
    }


show : TimeOfDay -> String
show time =
    String.fromInt time.hours ++ ":" ++ String.fromInt time.minutes


isSelected : Model -> Mission -> Bool
isSelected model mission =
    model.currentSelection == Just mission


validMission : List Mission -> Mission -> Bool
validMission missions toAdd =
    let
        totalDuration =
            toAdd
                :: missions
                |> List.map .duration
                |> List.sum
    in
    totalDuration < (60 * 7)



---- UPDATE ----


type SelectionTarget
    = Pool
    | Time Int


type Msg
    = AcceptMission Mission
    | Select Mission
    | Assign TimeOfDay Mission
    | ResignMission Mission


remove : a -> List a -> List a
remove element =
    List.filter (\el -> el /= element)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        AcceptMission thing ->
            ( { model
                | missionPool = remove thing model.missionPool
                , acceptedMissions = thing :: model.acceptedMissions
              }
            , Cmd.none
            )

        Select mission ->
            ( { model | currentSelection = Just mission }, Cmd.none )

        Assign time mission ->
            ( { model
                | currentSelection = Nothing
                , missionPool = remove mission model.missionPool
                , acceptedMissions = mission :: model.acceptedMissions
              }
            , Cmd.none
            )

        ResignMission thing ->
            ( { model
                | acceptedMissions = remove thing model.acceptedMissions
                , missionPool = thing :: model.missionPool
              }
            , Cmd.none
            )



---- VIEW ----


view : Model -> Html Msg
view model =
    div []
        [ img [ src "/logo.svg" ] []
        , h1 [] [ text "Mission pool" ]
        , div [] (List.map (viewMissionInPool model) model.missionPool)
        , h1 [] [ text "Planning" ]
        , viewSchedule model
        , h1 [] [ text "Legacy" ]
        , div [] (List.map viewAcceptedMission model.acceptedMissions)
        ]


oneHour : Model -> ( Bool, Int ) -> E.Element Msg
oneHour model ( first, time ) =
    let
        action =
            case model.currentSelection of
                Just mission ->
                    [ E.onClick (Assign { hours = time, minutes = 0 } mission) ]

                _ ->
                    []
    in
    E.el
        ([ EBd.solid
         , EBd.widthEach
            { top =
                if first then
                    1

                else
                    0
            , bottom = 1
            , left = 1
            , right = 1
            }
         , EBd.color (E.rgb 0 0 0)
         , E.width (E.px 400)
         , E.height (E.px 30)
         ]
            ++ action
        )
    <|
        E.el [ E.alignTop, E.paddingEach { top = 2, bottom = 0, left = 2, right = 0 }, EF.size 8 ] <|
            E.text <|
                String.fromInt time
                    ++ ":00"


viewSchedule : Model -> Html Msg
viewSchedule model =
    E.layout [ E.width E.fill ] <|
        E.column [ E.centerX ] <|
            List.map (oneHour model) <|
                ( True, 0 )
                    :: (List.map (\h -> ( False, h )) <| List.range 1 23)


viewMissionInPool : Model -> Mission -> Html Msg
viewMissionInPool model mission =
    let
        action =
            if validMission model.acceptedMissions mission then
                [ onClick (Select mission) ]

            else
                []

        highlight =
            if model.currentSelection == Just mission then
                [ style "color" "red" ]

            else
                []
    in
    div (action ++ highlight)
        [ span [] [ text mission.name ]
        , text ": "
        , span [] [ text mission.description ]
        , text " ("
        , span [] [ text ("happens: " ++ showFreq mission.every) ]
        , span [] [ text (", Duration: " ++ String.fromInt mission.duration) ]
        , text ")"
        ]


viewAcceptedMission : Mission -> Html Msg
viewAcceptedMission mission =
    div [ onClick (ResignMission mission) ]
        [ span [] [ text mission.name ]
        , text ": "
        , span [] [ text mission.description ]
        , text " ("
        , span [] [ text (String.fromInt mission.duration) ]
        , text ")"
        ]



---- PROGRAM ----


main : Program () Model Msg
main =
    Browser.element
        { view = view
        , init = \_ -> ( init, Cmd.none )
        , update = update
        , subscriptions = always Sub.none
        }

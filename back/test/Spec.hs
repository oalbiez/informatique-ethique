import           Lib
import           Data.Function                  ( (&) )
import           Data.Time
import           Test.Hspec              hiding ( after
                                                , before
                                                )

workday1 = WorkPeriod (LocalTime (fromGregorian 2020 1 1) (time 8 0))
                      (LocalTime (fromGregorian 2020 1 1) (time 18 0))

michon = mission "Lever Mme Michon" & during 1 0 & before 10 0
bidochon =
  mission "Repas M. Bidochon" & during 0 30 & after 11 30 & before 13 0


-- given_workday definition = Nothing
-- and_empty_workload = Nothing
-- when_I_validate_workload = Nothing
-- then_validation_should_be_success = Nothing

shouldGive :: Validator -> ValidationResult -> Expectation
shouldGive validator result = runValidator validator `shouldBe` result

time :: Int -> Int -> TimeOfDay
time hours minutes = TimeOfDay hours minutes 0

mission :: String -> Mission
mission description =
  Mission description (durationInTime 1 0) "" Nothing Nothing

during :: Int -> Int -> Mission -> Mission
during hours minutes el = el { m_duration = durationInTime hours minutes }

after :: Int -> Int -> Mission -> Mission
after hours minutes el = el { m_atEarliest = Just $ time hours minutes }

before :: Int -> Int -> Mission -> Mission
before hours minutes el = el { m_atLatest = Just $ time hours minutes }

address :: Address -> Mission -> Mission
address a el = el { m_address = a }


validateMissions wd missions = validateWorkLoad always15min wd missions

always15min :: Address -> Address -> Maybe Travel
always15min a1 a2 = Just (Travel a1 a2 (durationInTime 0 15))

main :: IO ()
main = hspec $ do
  describe "work day" $ do
    it "an empty workload should always be correct" $ do
      -- given_workday("8H00-18H00")
      -- and_empty_workload
      -- when_I_validate_workload
      -- then_validation_should_be_success
      validateMissions workday1 [] `shouldGive` Success

    it "a single mission occuring before start of workday should be incorrect"
      $ do
          let michon' = michon & before 7 0
          validateMissions workday1 [michon'] `shouldGive` MissionTooEarly

    it "a single mission occuring after end of workday should be incorrect" $ do
      validateMissions workday1 [michon & after 19 0]
        `shouldGive` MissionTooLate

    it "a mission occuring after end of workday should be incorrect" $ do
      validateMissions workday1 [bidochon, michon & after 19 0]
        `shouldGive` MissionTooLate

    it "a workday with too many missions should be incorrect" $ do
      validateMissions workday1 [bidochon & during 12 0]
        `shouldGive` MissionTooLong

    it "a workday with too many missions should be incorrect" $ do
      validateMissions workday1 [michon & during 4 0, bidochon & during 8 0]
        `shouldGive` MissionTooLong

    it "a mission beginning before the previous can finish should be incorrect"
      $ do
          validateMissions workday1
                           [michon & during 4 0, bidochon & before 11 45]
            `shouldGive` MissionTooEarly

    it "accepts correct work day" $ do
      validateMissions workday1 [michon, bidochon] `shouldGive` Success

    --it "rejects when mission + travel is too much" $ do
    --  validateMissions workday1 [michon & during 5 0, bidochon & during 5 0] `shouldGive` MissionTooLong

    --it "rejects when mission + travel is too much 2" $ do
    --  validateMissions workday1 [michon & during 1 0, bidochon & before 9 10] `shouldGive` MissionTooLong

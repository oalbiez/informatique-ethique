module Glasgow exposing (..)

import Element as E
import GenericTest as GT
import Html exposing (Html, a, button, div, h1, img, input, span, text)
import Html.Attributes exposing (src, style)
import Html.Events exposing (onClick, onInput)
import Maybe exposing (Maybe)


diagnostic : GT.Model -> E.Element msg
diagnostic model =
    let
        etatPatient =
            if GT.score model < 9 then
                "Traumatisme cranien grave"

            else if GT.score model < 13 then
                "Traumatisme cranien modéré"

            else
                "traumatisme cranien bénin"
    in
    E.row []
        [ E.text ("score : " ++ String.fromInt (GT.score model))
        , E.text " - "
        , E.text etatPatient
        ]


testYeux =
    { consigne = "Ouverture des yeux"
    , options =
        [ ( "Aucune", 1 )
        , ( "À la douleur", 2 )
        , ( "Au bruit", 3 )
        , ( "Spontanée", 4 )
        ]
    }


testItems =
    [ testYeux ]



--, testVerbal, testMoteur ]
